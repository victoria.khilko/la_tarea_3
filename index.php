<?php

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    if (!empty($_GET['save'])) {
        // Если есть параметр save, то выводим сообщение пользователю.
        //print('Спасибо, результаты сохранены.');
        print('Thanks, results saved');
    }
    // Включаем содержимое файла form.php.
    include('form.php');
    // Завершаем работу скрипта.
    exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['fio'])) {
    //print('Заполните имя.<br/>');
    print('Name is empty<br/>');
    $errors = TRUE;
}
if ((strlen($_POST['fio'])) < 5 | (strlen($_POST['fio'])) > 30){
    print('Name is word with length 5-30 symbols<br/>');
    $errors = TRUE;
}
if (empty($_POST['email'])) {
    print('Email is empty<br/>');
    $errors = TRUE;
}
if (empty($_POST['year'])) {
    print('Year is empty<br/>');
    $errors = TRUE;
}
if (empty($_POST['gender'])) {
    print('Gender is empty<br/>');
    $errors = TRUE;
}
if (empty($_POST['limbs'])) {
    print('Amount of limbs is empty<br/>');
    $errors = TRUE;
}
if (empty($_POST['abilities'])) {
    print('Abilities are empty<br/>');
    $errors = TRUE;
}
if (empty($_POST['bio'])) {
    print('Biography is empty<br/>');
    $errors = TRUE;
}
if (strlen($_POST['bio']) > 500){
    print('Bio is text less 500 symbols<br/>');
    $errors = TRUE;
}
if (empty($_POST['accept'])) {
    print('Check is empty<br/>');
    $errors = TRUE;
}

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
    // При наличии ошибок завершаем работу скрипта.
    exit();
}
$abilities = serialize($_POST['abilities']);
// Сохранение в базу данных.

$user = 'u16351';
$pass = '7947569';
$db = new PDO('mysql:host=localhost;dbname=u16351', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
    $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, abilities = ?, gender = ?, limbs = ?, bio = ?");
    $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['year'],$abilities,$_POST['gender'],$_POST['limbs'],$_POST['bio']));
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}

//  stmt - это "дескриптор состояния".

//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));

//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
 $stmt->bindParam(':firstname', $firstname);
 $stmt->bindParam(':lastname', $lastname);
 $stmt->bindParam(':email', $email);
 $firstname = "John";
 $lastname = "Smith";
 $email = "john@test.com";
 $stmt->execute();
 */

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');