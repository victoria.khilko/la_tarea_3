CREATE TABLE application (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL DEFAULT '',
  email varchar(128) NOT NULL DEFAULT '',
  year int(10) NOT NULL DEFAULT 0,
  abilities varchar(100) NOT NULL DEFAULT '',
  gender tinyint(1) NOT NULL DEFAULT 0,
  limbs tinyint(5) NOT NULL DEFAULT 0,
  bio varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
);